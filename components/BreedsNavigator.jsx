import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import BreedsList from "./BreedsList";
import BreedView from "./BreedView";

const BreedsStack = createNativeStackNavigator();

export default function BreedsNavigator() {
	return (
		<BreedsStack.Navigator>
			<BreedsStack.Screen
				name="BreedsList"
				component={BreedsList}
				options={() => ({
					title: 'Breeds'
				})}
			/>
			<BreedsStack.Screen
				name="BreedView"
				component={BreedView}
				options={({ route }) => {
					const title = route.params.screenName || route.params.breed
					return {
						title: title[0].toUpperCase() + title.substring(1)
					}
				}}
			/>
		</BreedsStack.Navigator>
	)
}