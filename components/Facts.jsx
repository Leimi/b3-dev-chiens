import React, {useEffect, useState} from 'react';
import { FlatList, View } from 'react-native';
import ListItemWithIcon from "./ListItemWithIcon";
import useStorageFavorites from "../api/useStorageFavorites";

const fetchFacts = () => {
	return fetch('https://manu.habite.la/mds/dog-facts.json').then(response => {
		return response.json()
	}).then(json => {
		return json.map(({fact}, index) => ({
			fact,
			number: index + 1
		}))
	})
}

export default function FactsScreen() {
	const [facts, setFacts] = useState([])
	useEffect(() => {
		fetchFacts().then(setFacts)
	}, [])

	const [favorites, setFavorites] = useStorageFavorites()

	return (
		<View style={{ flex: 1 }}>
			<FlatList
				data={facts}
				keyExtractor={({number}) => `${number}`}
				renderItem={({item}) => {
					const foundIndexInFavorites = favorites.findIndex(favorite => item.number === favorite.number)
					const isAlreadyInFavorites = foundIndexInFavorites !== -1
					const icon = isAlreadyInFavorites
						? 'heart'
						: 'heart-outline';
					return (
						<ListItemWithIcon icon={icon} onIconPress={() => {
							if (isAlreadyInFavorites) {
								favorites.splice(foundIndexInFavorites, 1)
								setFavorites([...favorites])
							} else {
								setFavorites([...favorites, item])
							}
						}}>
							{item.number}. {item.fact}
						</ListItemWithIcon>
					)
				}}
			/>
		</View>
	);
}
