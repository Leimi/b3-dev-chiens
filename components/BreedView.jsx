import React, { useState, useEffect} from "react";
import { ScrollView, Image } from "react-native";
import ListItem from './ListItem';

const fetchImages = (breed) => {
	return fetch(`https://dog.ceo/api/breed/${breed}/images/random/10`)
		.then(response => response.json())
		.then(json => json.message)
}

const fetchSubBreeds = (breed) => {
	return fetch(`https://dog.ceo/api/breed/${breed}/list`)
		.then(response => response.json())
		.then(json => json.code !== 404 ? json.message : [])
}

export default function BreedView({ navigation, route }) {
	const { breed, isSubBreed } = route.params
	const [images, setImages] = useState([])
	const [subBreeds, setSubBreeds] = useState([])

	useEffect(() => {
		fetchImages(breed).then(setImages)
		if (!isSubBreed) {
			fetchSubBreeds(breed).then(setSubBreeds)
		}
	}, [])

	const onSubBreedPress = (subBreed) => {
		navigation.push('BreedView', {
			breed: `${breed}/${subBreed}`,
			screenName: `${subBreed} (${breed})`,
			isSubBreed: true
		})
	}

	return (
		<ScrollView>
			{subBreeds.map(subBreed => (
				<ListItem key={subBreed} onPress={onSubBreedPress} item={subBreed} />
			))}

			{images.map(image => (
				<Image
					key={image}
					source={{ uri: image }}
					style={{width: '100%', height: 200}}
					resizeMode="contain"
				/>
			))}
		</ScrollView>
	)
}
