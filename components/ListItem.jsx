import { TouchableOpacity, Text } from "react-native";

export default function ListItem({ item, onPress }) {
	return (
		<TouchableOpacity
			style={{
				paddingVertical: 10,
				paddingHorizontal: 15
			}}
			onPress={() => {
				onPress(item)
			}}
		>
			<Text>
				{item[0].toUpperCase() + item.substring(1)}
			</Text>
		</TouchableOpacity>
	)
}
