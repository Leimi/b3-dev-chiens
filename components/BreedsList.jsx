import React, {useEffect, useState} from 'react';
import { FlatList } from 'react-native';
import ListItem from './ListItem';

const fetchBreeds = () => {
	return fetch('https://dog.ceo/api/breeds/list/all').then(response => {
		return response.json()
	}).then(json => Object.keys(json.message))
}

export default function BreedsList({ navigation }) {
	const [breeds, setBreeds] = useState([])
	useEffect(() => {
		fetchBreeds().then(setBreeds)
	}, [])
	const onBreedPress = (breed) => {
		navigation.navigate('BreedView', {
			breed
		})
	}
	return (
		<FlatList
			data={breeds}
			keyExtractor={(item, i) => `${item}${i}`}
			renderItem={({item}) => (
				<ListItem onPress={onBreedPress} item={item} />
			)}
		/>
	);
}
