import React from 'react';
import { FlatList } from 'react-native';
import ListItemWithIcon from './ListItemWithIcon';
import useStorageFavorites from "../api/useStorageFavorites";

export default function Favorites() {
	const [favorites, setFavorites] = useStorageFavorites()

	const onDeletePress = (item) => {
		const foundIndexInFavorites = favorites.findIndex(favorite => item.number === favorite.number)
		favorites.splice(foundIndexInFavorites, 1)
		setFavorites([...favorites])
	}
	return (
		<FlatList
			data={favorites}
			keyExtractor={(item, i) => `${item}${i}`}
			renderItem={({item}) => (
				<ListItemWithIcon onIconPress={onDeletePress} icon="trash">
					{item.number}. {item.fact}
				</ListItemWithIcon>
			)}
		/>
	);
}
