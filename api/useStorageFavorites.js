import { useState, useEffect, useCallback } from "react";
import { useFocusEffect } from "@react-navigation/native";
import AsyncStorage from '@react-native-async-storage/async-storage'

const getStorageFavorites = async () => {
	const favoritesJson = await AsyncStorage.getItem('favorites')
	return favoritesJson != null ? JSON.parse(favoritesJson) : []
}

const setStorageFavorites = (favorites) => {
	return AsyncStorage.setItem('favorites', JSON.stringify(favorites))
}

export default function useStorageFavorites() {
	const [favorites, setFavorites] = useState([])

	useFocusEffect(useCallback(() => {
		getStorageFavorites().then((storageFavorites) => {
			setFavorites(storageFavorites)
		})
	}, []))

	useEffect(() => {
		setStorageFavorites(favorites)
	}, [favorites])

	return [favorites, setFavorites]
}