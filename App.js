import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from '@expo/vector-icons';
import BreedsNavigator from "./components/BreedsNavigator";
import Facts from "./components/Facts";
import Favorites from "./components/Favorites";

const AppTabs = createBottomTabNavigator();
export default function App() {
	return (
	<NavigationContainer>
		<AppTabs.Navigator
			screenOptions={({ route }) => ({
				tabBarIcon: ({ focused, color, size }) => {
					let iconName;
					switch (route.name) {
						case 'Pictures':
							iconName = 'images'
							break;
						case 'Facts':
							iconName = 'book'
							break;
						case 'Favorites':
							iconName = 'heart'
							break;
					}
					return iconName
						? <Ionicons name={iconName} size={size} color={color} />
						: null;
				},
			})}
		>
			<AppTabs.Screen name="Pictures" component={BreedsNavigator} options={{headerShown: false}} />
			<AppTabs.Screen name="Facts" component={Facts} />
			<AppTabs.Screen name="Favorites" component={Favorites} />
		</AppTabs.Navigator>
	</NavigationContainer>
  );
}
